FROM arm32v7/ubuntu:bionic

ARG USER_ID="1000"
ARG GROUP_ID="1000"
ARG USER_NAME="pi"
ARG GROUP_NAME="raspberry"
ARG WIRINGPI=".docker-wiringpi"

COPY qemu-arm-static /usr/bin

RUN groupadd -g ${GROUP_ID} ${GROUP_NAME} && \
    useradd -l -u ${USER_ID} -g ${GROUP_NAME} ${USER_NAME} && \
    install -d -m 0755 -o ${USER_NAME} -g ${GROUP_NAME} /home/${USER_NAME}

RUN apt-get update && apt-get install -y \
    build-essential cmake g++ \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /home/${USER_NAME}
COPY ${WIRINGPI} wiringPi
WORKDIR wiringPi
RUN export WIRINGPI_SUDO='' && ./build
WORKDIR ../
RUN rm -rf wiringPi

RUN mkdir /data
RUN chown -R ${USER_NAME}:${GROUP_NAME} /data
VOLUME ["/data"]

USER ${USER_NAME}
WORKDIR /data