# Description
Debian-based image file used to compile C/C++ code for Raspberry Pi 2 & 3 micro-controllers from an x64 Linux system. It is recommended to pull the Docker image `gscar/raspberry-cpp`, but one may also build running the provided build script.

# Pull from Repository
```
docker pull gscar/raspberry-cpp
```

# Build
It is recommended to pull the image using the previous steps. Alternatively, if you wish to build the image yourself, please use the provided build script.
```
$ git clone https://gitlab.com/gscar-coppe-ufrj/docker/raspberry-cpp
$ cd raspberry-cpp
$ chmod +x build
$ ./build
```
This script will generate the Docker image and name it as `gscar/raspberry-cpp`.

# Run
To run this image from an x64 host machine, the `qemu-user-static` library must be installed. In a Debian-based system,
```
$ sudo apt install qemu-user-static
```
To run a command inside the container, while mounting the current directory in the container's `/data` directory, we recommend using the provided `docker-raspberry` script (don't forget to `chmod +x docker-raspberry`):
```
$ ./docker-raspberry [your command goes here]
```
Alternatively, if you wish to run everything by hand
```
$ docker run --rm \
    -v /usr/bin/qemu-arm-static:/usr/bin/qemu-arm-static \
    -v `pwd`:/data \
    gscar/raspberry-cpp \
    [your command goes here]
```
If the above command doesn't work, search for the correct location of `qemu-arm-static`. This can be automated using
```
$ docker run --rm \
    -v `whereis qemu-arm-static | cut -d ' ' -f2`:/usr/bin/qemu-arm-static \
    -v `pwd`:/data \
    gscar/raspberry-cpp \
    [your command goes here]
```

# License
This Software is distributed under the [MIT License](https://opensource.org/licenses/MIT).
